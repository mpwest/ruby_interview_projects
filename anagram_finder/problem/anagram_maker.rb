#!/usr/bin/env ruby

require 'optparse'

##########################
#      ANAGRAM MAKER
##########################
#
# Welcome to this coding challenge. A basic framework has been provided below.
# This is meant only to be a basic outline, feel free to modify, expand, remove,
# or change anything in this file. Good luck and don't forget to document your code.

def read_the_dictionary(dictionary_file)
  File.readlines(dictionary_file).each do |line|
    line
  end
end

def find_all_the_anagrams(dictionary)
  puts('Looking for anagrams...')
end

def most_anagrams(dictionary)
  puts('This word has the most anagrams')
end

def anagram?(word, dictionary)
  puts("Is #{word} an anagram?")
end

def print_stats(dictionary)
  puts('1...2...3...')
end

def parse_args
  options = {
    find_all: false,
    fancy: false,
    stats: false,
    single: false
  }

  word_to_check = ''

  parser = OptionParser.new do |opts|
    opts.banner = 'Usage: anagram_maker.rb [options]'
    opts.on('-a', '--anagrams', 'Find all the anagrams') do
      options[:find_all] = true
    end

    opts.on('-f', '--fancy', 'Find the word with the most anagrams') do
      options[:fancy] = true
    end

    opts.on('-s', '--stats', 'Print stats on all the anagrams found (use with -a flag)') do
      options[:stats] = true
    end

    opts.on('-w', '--word word', 'Pass in a word to check for anagrams') do |word|
      options[:single] = true
      word_to_check = word
    end

    opts.on('-h', '--help', 'Displays Help') do
      puts opts
      exit
    end
  end

  parser.parse!

  # If no flag was used then print a message and exit
  if options.values.all? { |value| value == false }
    puts('You must run this script with an argument. Run with --help for usage.')
    exit
  end

  { options: options, word: word_to_check }
end

def anagram_maker
  puts('### A NAG RAM! ###')

  opt_hash = parse_args

  # read all the words
  dictionary = read_the_dictionary('english_words.txt')

  if opt_hash[:options][:find_all]
    find_all_the_anagrams(dictionary)
    exit
  end

  if opt_hash[:options][:fancy]
    most_anagrams(dictionary)
    exit
  end

  if opt_hash[:options][:stats]
    print_stats(dictionary)
    exit
  end

  if opt_hash[:options][:single]
    anagram?(opt_hash[:word], dictionary)
    exit
  end
end

# Kick it all off
anagram_maker
