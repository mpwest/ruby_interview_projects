#!/usr/bin/env ruby

require 'optparse'

##########################
#      ANAGRAM MAKER
##########################
#
# Welcome to this coding challenge. A basic framework has been provided below.
# Please fill in the gaps and don't forget to document your code. Good luck.

# Read the dictionary file
#
# @param dictionary_file [String] path to the dictionary file
# @return [Array] returns an array of strings
def read_the_dictionary(dictionary_file)
  File.readlines(dictionary_file).map do |line|
    clean = line.strip
    clean if clean.length >= 4
  end
end

# Alphabetizes the characters in a string
#
# For example, "dangling" becomes "adggilnn"
# @param [String] A string to sort
# @return [String] A newly sorted string
def sort_a_string(string_to_sort)
  string_to_sort.split('').sort.join
end

# Anagram Hasher
#
# Break each word into characters and alphabetize it. Then add the
# new word to a hash as the key where the value is an array of those words.
# For example, the words:
# words = [part, prat, rapt]
# hash = {aprt : [part, prat, rapt]}
#
# This new hash can be used to store anagrams as we find them.
#
# @param [List] dictionary is a list of strings
# @return [Hash] returns a hash of sorted keystrings and anagrams
def anagram_hasher(dictionary)
  puts('Reading in the dictionary...')

  words_hash = {}

  dictionary.each do |cur_word|
    word = cur_word.downcase.delete("\n")
    sorted_characters = sort_a_string(word)

    words_array = []
    if words_hash.has_key?(sorted_characters)
      words_array = words_hash[sorted_characters]
      words_array.append(word)
      words_hash[sorted_characters] = words_array
    else
      words_hash[sorted_characters] = [word]
    end
  end

  words_hash
end

# Find all the anagrams
#
# Go through the entire hash and print only words with anagrams
#
# @params [Hash] The hash of all the words and their anagrams
def find_all_the_anagrams(new_words_hash)
  # print only the anagrams
  new_words_hash.each do |key, value|
    next unless value.length > 1

    # print it nicely
    puts("%20s #{value}" % value[0])
  end
end

# Find which word makes the most anagrams
#
# @param [Hash] The hash of all the words and their anagrams
def most_anagrams(new_words_hash)
  # most anagrams
  most_anagrams = 0
  largest_anagram = ''

  new_words_hash.each do |key, value|
    next unless value.length > 1

    if value.length > most_anagrams
      most_anagrams = value.length
      largest_anagram = key
    end
  end

  puts("This word has the most anagrams: #{largest_anagram} (#{most_anagrams})")
  new_words_hash[largest_anagram].each do |anagram|
    puts("  #{anagram}")
  end
end

# Check if a word is an anagram
#
# @param [String] The word to check
def anagram?(word, new_words_hash)
  word = word.downcase.delete("\n")
  dorw = sort_a_string(word)
  puts("Is '#{word}' an anagram?")

  if new_words_hash[dorw].length > 1
    puts("Yes it is : #{new_words_hash[dorw]}")
  else
    puts('No, it is not')
  end
end

# Print some stats on our words
#
# @param [Hash] The hash of all the words and their anagrams
def print_stats(new_words_hash)
  longest_anagram = ''
  word_count = 0
  anagram_count = 0

  new_words_hash.each do |key, value|
    # find the longest word that has an anagram
    longest_anagram = key if value.length > 1 && (key.length > longest_anagram.length)

    # do some counting
    word_count += 1
    anagram_count += 1 if value.length > 1
  end

  anagram_percentage = (anagram_count.to_f / word_count * 100.0).round(2)

  puts("Total Words    : #{word_count}")
  puts("Total Anagrams : #{anagram_count}")
  puts("Anagram %      : #{anagram_percentage}")

  puts("The largest anagrams are #{new_words_hash[longest_anagram]}")
end

def parse_args
  options = {
    find_all: false,
    fancy: false,
    stats: false,
    single: false
  }

  word_to_check = ''

  parser = OptionParser.new do |opts|
    opts.banner = 'Usage: anagram_maker.rb [options]'
    opts.on('-a', '--anagrams', 'Find all the anagrams') do
      options[:find_all] = true
    end

    opts.on('-f', '--fancy', 'Find the word with the most anagrams') do
      options[:fancy] = true
    end

    opts.on('-s', '--stats', 'Print stats on all the anagrams found (use with -a flag)') do
      options[:stats] = true
    end

    opts.on('-w', '--word word', 'Pass in a word to check for anagrams') do |word|
      options[:single] = true
      word_to_check = word
    end

    opts.on('-h', '--help', 'Displays Help') do
      puts opts
      exit
    end
  end

  parser.parse!

  # If no flag was used then print the help and exit
  if options.values.all? { |value| value == false }
    puts('You must run this script with an argument. Run with --help for usage.')
    exit
  end

  { options: options, word: word_to_check }
end

def anagram_maker
  puts('### A NAG RAM! ###')

  opt_hash = parse_args

  # read all the words
  dictionary = read_the_dictionary('english_words.txt').reject { |w| w.nil? }

  # sort the hash
  new_words_hash = anagram_hasher(dictionary)

  if opt_hash[:options][:find_all]
    find_all_the_anagrams(new_words_hash)
    exit
  end

  if opt_hash[:options][:fancy]
    most_anagrams(new_words_hash)
    exit
  end

  if opt_hash[:options][:stats]
    print_stats(new_words_hash)
    exit
  end

  if opt_hash[:options][:single]
    anagram?(opt_hash[:word], new_words_hash)
    exit
  end
end

# Kick it all off
anagram_maker
