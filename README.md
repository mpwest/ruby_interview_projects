# README #

This is a repo for collecting the various programming interview questions that we use for testing potential Ruby developers.

## Current Problems ##
This is a list of the various programming problems and their solutions. 

### Anagram Finder ###
Find all the anagrams in the provided dictionary

### Haiku Maker (in progress) ###
Generate your own haikus from the provided dictionary


### Feedback ###
For questions or other feedback contact James Stoup or Clint Little.
